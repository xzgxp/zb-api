package zb.md2html.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
 * 请求参数
 * @author Administrator
 *
 */
@Data
public class Table {
	private Line tr;
	private List<Line> tds;

	@Data
	@AllArgsConstructor
	public static class Line {
		private String param1;
		private String param2;
		private String param3;
	}
}