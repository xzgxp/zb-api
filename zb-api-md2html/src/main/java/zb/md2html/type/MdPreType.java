package zb.md2html.type;

public enum MdPreType {
	/** 请求json */
	request,
	/** 返回json */
	response,
	/** 相应数据 */
	resdata,

	/** 普通代码块 */
	json,
	/** url请求 */
	curl,
	
	/** 对应语言的代码块 */
	java,python,php,golang,ruby

}