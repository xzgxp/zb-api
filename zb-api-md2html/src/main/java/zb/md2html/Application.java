package zb.md2html;

import java.io.IOException;

import lombok.val;

/**
 * 运行主方法
 * 
 * @author Administrator
 *
 */
public class Application {
	public static void main(String[] args) throws IOException {
		// vue项目的根目录
		val projectPath = "E:\\zb_node\\";
		// 执行生成
		new CoreGen(projectPath).codeGen();
	}
}
