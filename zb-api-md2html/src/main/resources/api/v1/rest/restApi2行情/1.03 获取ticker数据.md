## 获取ticker数据
```request
GET http://api.zb.cn/data/v1/ticker?market=btc_usdt
```
```response
{
    "ticker": {
        "vol": "40.463", 
        "last": "0.899999", 
        "sell": "0.5",
        "buy": "0.225", 
        "high": "0.899999", 
        "low": "0.081"
    }, 
    "date": "1507875747359"
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
market | String | 例：btc_qc

```resdata
high : 最高价
low : 最低价
buy : 买一价
sell : 卖一价
last : 最新成交价
vol : 成交量(最近的24小时)
```

```java
public void getTicker() {
	//得到返回结果
	String returnJson = HttpRequest.get("http://api.zb.cn/data/v1/ticker?market=btc_usdt").send().bodyText();
}
```

```python
def get(self, url):
    while True:
        try:
            r = requests.get(url)
        except Exception:
            time.sleep(0.5)
            continue
        if r.status_code != 200:
            time.sleep(0.5)
            continue
        r_info = r.json()
        r.close()
        return r_info
        
def getTicker(self, market):
    url = 'http://api.zb.cn/data/v1/ticker?market=btc_usdt'
    return self.get(url)
```