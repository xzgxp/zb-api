## 获取借贷记录
```request
GET https://trade.zb.cn/api/getLoanRecords?accesskey=youraccesskey&marketName=btsqc&method=getLoanRecords&pageIndex=1&pageSize=10&status=2&sign=请求加密签名串&reqTime=当前时间毫秒数
```
```response
{
	"code"                 : 1000,
	"message"              : "操作成功",
	"result"               : [
		{
			"createTime"           : 1522473159000,
			"statusShow"           : "还款中",
			"freezId"              : "0",
			"tstatus"              : 0,
			"withoutLxAmount"      : "0",
			"investMark"           : false,
			"withoutLxDays"        : 0,
			"hasRepay"             : "0",
			"amount"               : "0.10008001",
			"id"                   : 290,
			"fwfScale"             : "0.2",
			"rate"                 : "0.0005",
			"marketName"           : "btsqc",
			"hasLx"                : "0",
			"isIn"                 : false,
			"balanceAmount"        : "0",
			"fundType"             : 15,
			"outUserId"            : 110797,
			"inUserId"             : 110797,
			"repayDate"            : 1523337159000,
			"zheLx"                : "0",
			"outUserFees"          : "",
			"dikouLx"              : "0",
			"sourceType"           : 8,
			"coinName"             : "QC",
			"reward"               : "0",
			"status"               : 1,
			"arrearsLx"            : "0.00005004",
			"balanceWithoutLxDays" : 0,
			"riskManage"           : 1,
			"rateAddVal"           : "0",
			"outUserName"          : "18022121179",
			"inUserName"           : "18022121179",
			"inUserLock"           : false,
			"rateForm"             : 1,
			"loanId"               : 94,
			"nextRepayDate"        : 1522559559000
		}
		...
	]
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
method | String | 直接赋值 getLoanRecords
accesskey | String | accesskey
loanId | String | 理财id（借出就传 借入不用传）
marketName | String | 市场
status | int | 状态(1还款中 2已还款 3 需要平仓 4 平仓还款 5自动还款中 6自动还款)
pageIndex | int | 当前页数
pageSize | int | 每页数量
sign | String | 请求加密签名串
reqTime | long | 当前时间毫秒数

```resdata
code : 返回代码
message : 提示信息
result :
id	    : 借贷记录ID
loanId	: 理财ID
inUserId	: 借入者ID
inUserName	: 借入者用户名
outUserId	: 借出者ID
outUserName	: 借出者用户名
fundType	: 币种类型
amount	    : 借入金额
status	    : 状态 1还款中 2已还款 3需要平仓 4 平仓还款 5自动还款
createTime	: 借贷成功时间
reward	: 奖励金额
balanceAmount	: 投标后借款剩余金额
rate	: 利率
hasRepay	:已还本金金额
hasLx	: 已还利息
dikouLx	: 已抵扣利息
zheLx	: 折算线上价
arrearsLx	: 拖欠利息
nextRepayDate	: 下次还款时间
riskManage	: 风险控制
inUserLock	: 借入者是否被锁定
withoutLxAmount	: 免息额度
withoutLxDays	: 免息天数
balanceWithoutLxDays	: 剩余的免息天数
rateForm	: 利率形式
rateAddVal	: 增长幅度
repayDate	: 还款时间
marketName	: 市场
fwfScale	: 服务费费率
investMark	: 自动续借
sourceType	: 来源类型： 8”网页”，5”手机APP”，6”接口API”
```

```java
public void getLoanRecords() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String digest = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String paramStr = "accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&loanId=1545454415&marketName=btsqc&method=getLoanRecords&pageIndex=1&pageSize=10&status=1";
	//sign通过HmacMD5加密得到:08891e23a060aa0f979332939e7e845d
	String sign = EncryDigestUtil.hmacSign(paramStr, digest);
	//组装最终发送的请求url
	String finalUrl = "https://trade.zb.cn/api/getLoanRecords?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&loanId=1545454415&marketName=btsqc&method=getLoanRecords&pageIndex=1&pageSize=10&reqTime=1539942326078&sign=08891e23a060aa0f979332939e7e845d&status=1";
	//取得返回结果json
	String resultJson = HttpRequest.get(finalUrl).send().bodyText();
}
```

```python
import hashlib
import struct
import time
import requests

class ZApi:
    def __init__(self, access_key, secret_key):
        self._access_key_ = access_key
        self._secret_key_ = secret_key
        self._markets_ = self.markets()
        if len(self._markets_) < 1:
            raise Exception("Get markets status failed")

    def getLoanRecords(self, market, type, amount, price):
        MyUrl = 'https://trade.zb.cn/api/getLoanRecords'
        params = 'accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&loanId=1545454415&marketName=btsqc&method=getLoanRecords&pageIndex=1&pageSize=10&status=1'
        return self.call_api(url=MyUrl, params=params)

    def call_api(self, url, params=''):
        full_url = url
        if params:
            #sha_secret=86429c69799d3d6ac5da5c2c514baa874d75a4ba
            sha_secret = self.digest(self._secret_key_)
            #sign = 08891e23a060aa0f979332939e7e845d
            sign = self.hmac_sign(params, sha_secret)
            req_time = int(round(time.time() * 1000))
            params += '&sign=%s&reqTime=%d' % (sign, req_time)
            #full_url = https://trade.zb.cn/api/getLoanRecords?accesskey=ce2a18e0-dshs-4c44-4515-9aca67dd706e&loanId=1545454415&marketName=btsqc&method=getLoanRecords&pageIndex=1&pageSize=10&reqTime=1540295995578&sign=08891e23a060aa0f979332939e7e845d&status=1
            full_url += '?' + params
        result = {}
        while True:
            try:
                r = requests.get(full_url, timeout=2)
            except Exception:
                time.sleep(0.5)
                continue
            if r.status_code != 200:
                time.sleep(0.5)
                r.close()
                continue
            else:
                result = r.json()
                r.close()
                break
        return result

    @staticmethod
    def fill(value, lenght, fill_byte):
        if len(value) >= lenght:
            return value
        else:
            fill_size = lenght - len(value)
        return value + chr(fill_byte) * fill_size

    @staticmethod
    def xor(s, value):
        slist = list(s.decode('utf-8'))
        for index in range(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)

    def hmac_sign(self, arg_value, arg_key):
        keyb = struct.pack("%ds" % len(arg_key), arg_key.encode('utf-8'))
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        k_ipad = self.xor(keyb, 0x36)
        k_opad = self.xor(keyb, 0x5c)
        k_ipad = self.fill(k_ipad, 64, 54)
        k_opad = self.fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad.encode('utf-8'))
        m.update(value)
        dg = m.digest()

        m = hashlib.md5()
        m.update(k_opad.encode('utf-8'))
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg
  
    def digest(self, arg_value):
        value = struct.pack("%ds" % len(arg_value), arg_value.encode('utf-8'))
        h = hashlib.sha1()
        h.update(value)
        dg = h.hexdigest()
        return dg
```