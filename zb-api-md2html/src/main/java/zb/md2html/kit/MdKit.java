package zb.md2html.kit;

import java.io.File;
import java.io.IOException;

import org.markdownj.MarkdownProcessor;

import jodd.io.FileUtil;
import jodd.jerry.Jerry;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MdKit {
	/**
	 * md文件读取
	 * @param mdFile
	 * @return
	 * @throws IOException
	 */
	public static String readMd2Str(File mdFile) throws IOException {
		val md = FileUtil.readString(mdFile);
		val markdownProcessor = new MarkdownProcessor();
		val html = markdownProcessor.markdown(md);
		// 加入id
		val doc = Jerry.jerry(html);
		val h1 = doc.$("h1");
		h1.attr("id", h1.text());

		val h2 = doc.$("h2");
		log.debug(h2.text());
		h2.attr("id", h2.text());
		return doc.html();
	}
}
